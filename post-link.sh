if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper fsl_mrs fsl_mrsi fsl_mrs_preproc fsl_mrs_preproc_edit fsl_mrs_proc fsl_mrs_sim mrs_tools basis_tools merge_mrs_reports svs_segment mrsi_segment results_to_spectrum
fi
